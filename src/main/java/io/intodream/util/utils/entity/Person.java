package io.intodream.util.utils.entity;

/**
 * {描述}
 *
 * @author yangxianxi@gogpay.cn
 * @date 2018/7/13 10:29
 */

public class Person {

    private String name;

    private String phone;

    private Integer age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Person(String name, String phone, Integer age) {
        this.name = name;
        this.phone = phone;
        this.age = age;
    }
}
